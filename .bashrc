# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# enable programmable completion features
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# check if there is anything to update
if [ $(which dateutils.ddiff) ]; then
    last_update_date=$(grep -B 1 -E "apt.*upgrade" /var/log/apt/history.log | tail -n2 | head -n1 | sed "s/^Start-Date: //") 
    no_day_since_last_update=$(dateutils.ddiff "$(date -I)" "$last_update_date")
    if [ "$no_day_since_last_update" -gt 6 ]; then
        echo -e "\033[1;31mSystem was updated $no_day_since_last_update days ago!"
    fi
else
    echo "Please install dateutils to be able to check last upgrade time"
fi

function ddev {
    local language=$1
    shift
    docker container run --rm -it $@ -v `pwd`:/home/developer/workspace vargab95/nvim-$language
}

# search in files with a specific extension
function extsearch () {
    grep -sRn --include=*.$1 $2
}

function print_prompt {
    local EXIT="$?"
    PS1="\e[0m\n"
    if [ $EXIT != 0 ]; then
        PS1+="\e[31m ✘ ${EXIT} "
    else
        PS1+="\e[32m ✔  "
    fi

    PS1+="\e[1;33m`date +%H:%M` "

    local IS_GIT=$(git rev-parse --is-inside-work-tree 2>&1)
    if [[ "$IS_GIT" == "true" ]]; then
        local BRANCH=$(git rev-parse --abbrev-ref HEAD)
        PS1+="\e[1;34m[⎇ $BRANCH] "
    fi

    PS1+="\e[1;32m\w"
    PS1+=" \e[32m\n▶ "
}

export PROMPT_COMMAND='print_prompt;RETRN_VAL=$?;dbhistory -a "$(history | tail -n1 | sed "s/^[ ]*[0-9]*[ ]*//" )"'
export PATH="$HOME/.joplin/:/var/lib/flatpak/exports/bin/:$HOME/devtools/flutter/bin/:$HOME/devtools/go/bin/:$HOME/devtools/zig/:$HOME/.bin:$HOME/.config/herd-lite/bin:$PATH"
export CHROME_EXECUTABLE="/var/lib/flatpak/exports/bin/com.google.Chrome"
export XKB_DEFAULT_LAYOUT=hu
export EDITOR=nvim
export BROWSER=firefox
export TERM=ghostty

source ~/.secrets.sh
source ~/.bash_aliases
source ~/.cargo/env

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH
export PATH="$PATH":"$HOME/.pub-cache/bin"

if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
    export XDG_CURRENT_DESKTOP=sway
    export QT_QPA_PLATFORM=wayland
fi

export PHP_INI_SCAN_DIR="$HOME/.config/herd-lite/bin:$PHP_INI_SCAN_DIR"
