# Devenv

Config files for my development environment.

## Installation

Check it out with the following command.
```bash
git clone --bare https://gitlab.com/vargab95/devenv.git $HOME/.dotfiles/
```

Then run the following commands to checkout the files.
```bash
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME checkout
```

## Configuration

As you may not want to store all of your files in your home folder in git, you
can disable showing untracked files in the status command output by executing
the following command.

```bash
dotconf config --local status.showUntrackedFiles no
```

For third party integrations, like the gdb dashboard, please execute the
following command.

```bash
dotconf submodule update --init --recursive
```
