# editors
alias vi=nvim
alias vim=nvim
alias vik='NVIM_APPNAME="nvim-kickstart" nvim'
alias vimk='NVIM_APPNAME="nvim-kickstart" nvim'
alias nvimk='NVIM_APPNAME="nvim-kickstart" nvim'

# git stuff
alias dotconf='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias gitsubmo='git submodule update --recursive --remote'

# dockerized dev environments
alias ddevnode='docker container run --rm -it -u $(id -u ${USER}):$(id -g ${USER}) -v `pwd`:/app -v $HOME/.npmrc:/root/.npmrc -w /app node:20.15 bash'
alias ddevpython='docker run -it --rm -u $(id -u ${USER}):$(id -g ${USER}) -v `pwd`:/app -w /app python:3.10 /bin/bash'

# system control
alias suspend='systemctl suspend'

# cdev
alias cmake_debug='cmake -DCMAKE_BUILD_TYPE=Debug'
alias cmake_release='cmake -DCMAKE_BUILD_TYPE=Release'
alias valgrind_all='valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes'

# misc
alias detoen='~/.local/bin/argos-translate --from-lang de --to-lang en'
alias updateall='sudo apt update -y && sudo apt upgrade -y && flatpak update -y && sudo npm update -g'

# flutter dev
alias start_emulator='QT_QPA_PLATFORM=xcb flutter emulator --launch pixel_5'
