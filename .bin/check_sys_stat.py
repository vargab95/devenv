#!/usr/bin/python3

import csv

with open(".sys_stats.log", newline="") as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        virtual_memory = int(row[-3])
        virtual_memory_in_gbs = virtual_memory / (1024 * 1024 * 1024)
        if virtual_memory_in_gbs >= 16:
            print("At", row[0], "was", virtual_memory_in_gbs, "GB")
