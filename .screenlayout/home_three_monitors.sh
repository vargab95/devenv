#!/bin/sh
xrandr --output eDP-1 --primary --mode 1920x1080 --pos 2944x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output DVI-I-4-4 --off --output DVI-I-3-3 --off --output DVI-I-2-2 --mode 1920x1200 --pos 1024x0 --rotate normal --output DVI-I-1-1 --mode 1280x1024 --pos 0x0 --rotate left
