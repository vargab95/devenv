# Change the argument to True to still load settings configured via autoconfig.yml
config.load_autoconfig(False)

# Disable autoplaying videos
config.set('content.autoplay', False, '*')

# Disable websites to request geolocations.
config.set('content.geolocation', False)

# Disable websites to show notifications.
config.set('content.notifications.enabled', False)

# Which cookies to accept.
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')

# User agent to send.
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')

# Load images automatically in web pages.
config.set('content.images', True, 'chrome-devtools://*')
config.set('content.images', True, 'devtools://*')

# Enable JavaScript.
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')
config.set('content.javascript.enabled', True, 'https://gitlab.com/*')

# Allow websites to share screen content.
config.set('content.desktop_capture', True, 'https://meet.google.com')

# Allow websites to record audio.
config.set('content.media.audio_capture', True, 'https://meet.google.com')
config.set('content.media.audio_capture', True, 'https://www.messenger.com')

# Allow websites to record audio and video.
config.set('content.media.audio_video_capture', True, 'https://meet.google.com')
config.set('content.media.audio_video_capture', True, 'https://www.messenger.com')

# Allow websites to record video.
config.set('content.media.video_capture', True, 'https://meet.google.com')
config.set('content.media.video_capture', True, 'https://www.messenger.com')

# Allow google ads on google, because sometimes it brings the best match
config.set('content.blocking.enabled', False)

# Set the startup and default page
config.set('url.start_pages', ['https://www.startpage.com/'])
config.set('url.default_page', 'https://www.startpage.com/')
