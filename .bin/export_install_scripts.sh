#!/bin/bash

tmp=$(flatpak list --app --columns=application | xargs echo -n) && \
    echo -e '#!/bin/bash'"\nflatpak install "$tmp > ~/.pkglist/flatpak/install_flatpaks.sh && \
    sudo chmod +x ~/.pkglist/flatpak/install_flatpaks.sh

echo "apt install `apt-mark showmanual | xargs`" > ~/.pkglist/debian/install_packages.sh
