import os
import csv   
import psutil
import datetime
import json

fields = [
    datetime.datetime.now().isoformat(),
    psutil.cpu_percent(interval=3),
    json.dumps(psutil.cpu_percent(interval=3, percpu=True)),
    psutil.swap_memory().used,
    psutil.virtual_memory().used,
    psutil.disk_usage("/").used,
    psutil.disk_usage("/home").used
]

with open(os.getenv("HOME") + "/.sys_stats.log", "a") as f:
    writer = csv.writer(f)
    writer.writerow(fields)
